﻿<?xml version='1.0' encoding='UTF-8'?>
<Library LVVersion="17008000">
	<Property Name="NI.Lib.Icon" Type="Bin">&amp;Q#!!!!!!!)!"1!&amp;!!!-!%!!!@````]!!!!"!!%!!!(^!!!*Q(C=\&gt;8"&lt;2MR%!813:"$A*T51;!7JA7VI";G"6V^6!P4AFJ1#^/#7F!,TN/'-(++=IC2(-TVS+O`80+:3[QDNP9VYEO]0GP@@NM_LD_\`K4&amp;2`NI`\;^0.WE\\ZH0]8D2;2'N3K6]:DK&gt;?1D(`H)2T\SFL?]Z3VP?=N,8P+3F\TE*5^ZSF/?]J3H@$PE)1^ZS*('Z'/C-?A99(2'C@%R0--T0-0D;QT0]!T0]!S0,D%]QT-]QT-]&lt;IPB':\B':\B-&gt;1GG?W1]QS0Y;.ZGK&gt;ZGK&gt;Z4"H.UQ"NMD:Q'Q1DWM6WUDT.UTR/IXG;JXG;JXF=DO:JHO:JHO:RS\9KP7E?BZT(-&amp;%]R6-]R6-]BI\C+:\C+:\C-6U54`%52*GQ$)Y1Z;&lt;3I8QJHO,R+YKH?)KH?)L(J?U*V&lt;9S$]XDE0-E4`)E4`)EDS%C?:)H?:)H?1Q&lt;S:-]S:-]S7/K3*\E3:Y%3:/;0N*A[=&lt;5+18*YW@&lt;,&lt;E^J&gt;YEO2U2;`0'WJ3R.FOM422L=]2[[,%?:KS(&amp;'PR9SVKL-7+N1CR`LB9[&amp;C97*0%OPH2-?Y_&lt;_KK,OKM4OKI$GKP&gt;I^&lt;`X,(_`U?N^MNLN&gt;L8#[8/*`0=4K&gt;YHA]RO&amp;QC0V_(\P&gt;\OUV].XR^E,Y_6Z[=@YH^5\`3`_$&gt;W.]DF`(N59`!/&lt;!-PQ!!!!!</Property>
	<Property Name="NI.Lib.SourceVersion" Type="Int">385908736</Property>
	<Property Name="NI.Lib.Version" Type="Str">1.0.0.0</Property>
	<Item Name="Determine Clicked Array Element" Type="Folder">
		<Item Name="Sub Vis" Type="Folder">
			<Item Name="Determine Clicked Array Element Index - Calculate Gapped and Un-Gapped Coordinates.vi" Type="VI" URL="../Determine Clicked Array Element/Methods/Determine Clicked Array Element Index - Calculate Gapped and Un-Gapped Coordinates.vi"/>
			<Item Name="Determine Clicked Array Element Index - Calculate Scrollbar Edges.vi" Type="VI" URL="../Determine Clicked Array Element/Methods/Determine Clicked Array Element Index - Calculate Scrollbar Edges.vi"/>
			<Item Name="Determine Clicked Array Element Index - Cancel Error Code 1320.vi" Type="VI" URL="../Determine Clicked Array Element/Methods/Determine Clicked Array Element Index - Cancel Error Code 1320.vi"/>
			<Item Name="Determine Clicked Array Element Index - Determine Array and Scrollbar Properties.vi" Type="VI" URL="../Determine Clicked Array Element/Methods/Determine Clicked Array Element Index - Determine Array and Scrollbar Properties.vi"/>
			<Item Name="Determine Clicked Array Element Index - Determine Array Element Coordinates.vi" Type="VI" URL="../Determine Clicked Array Element/Methods/Determine Clicked Array Element Index - Determine Array Element Coordinates.vi"/>
			<Item Name="Determine Clicked Array Element Index - Determine if Item Clicked.vi" Type="VI" URL="../Determine Clicked Array Element/Methods/Determine Clicked Array Element Index - Determine if Item Clicked.vi"/>
			<Item Name="Determine Clicked Array Element Index - Determine Index Values (1D or 2D Array).vi" Type="VI" URL="../Determine Clicked Array Element/Methods/Determine Clicked Array Element Index - Determine Index Values (1D or 2D Array).vi"/>
			<Item Name="Determine Clicked Array Element Index - Get Minimal Array Properties.vi" Type="VI" URL="../Determine Clicked Array Element/Methods/Determine Clicked Array Element Index - Get Minimal Array Properties.vi"/>
			<Item Name="Determine Clicked Array Element Index - Is Array Element Gap Displayed.vi" Type="VI" URL="../Determine Clicked Array Element/Methods/Determine Clicked Array Element Index - Is Array Element Gap Displayed.vi"/>
			<Item Name="Determine Clicked Array Element Index - Test For Array Element Gap.vi" Type="VI" URL="../Determine Clicked Array Element/Methods/Determine Clicked Array Element Index - Test For Array Element Gap.vi"/>
			<Item Name="Determine Clicked Array Element Index - Test For Error Code 1320.vi" Type="VI" URL="../Determine Clicked Array Element/Methods/Determine Clicked Array Element Index - Test For Error Code 1320.vi"/>
		</Item>
		<Item Name="Determine Clicked Array Element Index.vi" Type="VI" URL="../Determine Clicked Array Element/Determine Clicked Array Element Index.vi"/>
	</Item>
	<Item Name="Generic Objects" Type="Folder">
		<Property Name="NI.SortType" Type="Int">3</Property>
		<Item Name="Fix in Place" Type="Folder">
			<Item Name="Fix to top-right.vi" Type="VI" URL="../Fix to top-right.vi"/>
			<Item Name="Fix to top-left.vi" Type="VI" URL="../Fix to top-left.vi"/>
			<Item Name="Fix to Center.vi" Type="VI" URL="../Fix to Center.vi"/>
		</Item>
		<Item Name="HoverOverTab.vi" Type="VI" URL="../HoverOverTab.vi"/>
		<Item Name="Open on Click (Left_Right).vi" Type="VI" URL="../Open on Click (Left_Right).vi"/>
		<Item Name="Open on Click (Up_Down).vi" Type="VI" URL="../Open on Click (Up_Down).vi"/>
		<Item Name="Set Enabled States.vi" Type="VI" URL="../Set Enabled States.vi"/>
		<Item Name="Set visible States.vi" Type="VI" URL="../Set visible States.vi"/>
	</Item>
	<Item Name="Images" Type="Folder">
		<Item Name="Resize 2D Image.vi" Type="VI" URL="../Images/Resize 2D Image.vi"/>
	</Item>
	<Item Name="Smooth Scroll Array" Type="Folder">
		<Item Name="Panel Set.vi" Type="VI" URL="../Smooth Scroll Array/Panel Set.vi"/>
		<Item Name="Scroll.vi" Type="VI" URL="../Smooth Scroll Array/Scroll.vi"/>
	</Item>
</Library>
